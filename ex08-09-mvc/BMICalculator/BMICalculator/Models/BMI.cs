﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMICalculator.Models
{
    public class Bmi
    {
        public int Height { get; set; }
        public int Weight { get; set; }
        
    }
}
