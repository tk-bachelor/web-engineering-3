﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BMICalculator.Models;
using BMICalculator.Services;
using Microsoft.AspNetCore.Mvc;

namespace BMICalculator.Controllers
{
    public class HomeController : Controller
    {
        private IBmiService _bmiService;

        public HomeController()
        {
            this._bmiService = new BmiService();
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(Bmi bmi)
        {
            return Content(this._bmiService.Calculate(bmi).ToString());
        }

        public IActionResult Ajax()
        {
            ViewData["Message"] = "You application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
