﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BMICalculator.Models;

namespace BMICalculator.Services
{
    public class BmiService : IBmiService
    {
        public double Calculate(Bmi bmi)
        {
            double result = bmi.Weight / Math.Pow(x: Convert.ToDouble(value: bmi.Height) / 100, y: 2);
            return Math.Round(value: result, digits: 2);
        }
    }
}
