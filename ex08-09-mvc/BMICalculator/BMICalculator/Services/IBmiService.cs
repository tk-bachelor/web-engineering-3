﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BMICalculator.Models;

namespace BMICalculator.Services
{
    interface IBmiService
    {
        double Calculate(Bmi bmi);
    }
}
