/**
 * Created by mstolze on 19/02/17.
 */
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var p = { x: 1, y: 2 }; // TS Error: [  ] Ja [X ] Nein
//p.x = 2; 			       // TS Error: [X ] Ja [  ] Nein
p.y = 9; // TS Error: [  ] Ja [X ] Nein
//p.z = 7;                   // TS Error: [  ] Ja [X ] Nein
var Counter = (function () {
    function Counter(_a) {
        var _b = _a === void 0 ? {} : _a, _c = _b.doors, doors = _c === void 0 ? 2 : _c, _d = _b.basePrice, basePrice = _d === void 0 ? 40 : _d;
        this._doors = doors;
        this._basePrice = basePrice;
    }
    Object.defineProperty(Counter.prototype, "price", {
        get: function () {
            var priceFactor = Counter.DOOR_FACTOR * this._doors / 100;
            return priceFactor * this._basePrice;
        },
        enumerable: true,
        configurable: true
    });
    Counter.prototype.priceString = function () {
        return "CHF: " + this.price;
    };
    Counter.DOOR_FACTOR = 2;
    return Counter;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Counter;
var FancyCounter = (function (_super) {
    __extends(FancyCounter, _super);
    function FancyCounter() {
        _super.apply(this, arguments);
    }
    FancyCounter.DOOR_FACTOR = 3;
    return FancyCounter;
}(Counter));
var c = new Counter({ basePrice: 50 });
console.log("5.1.1", c.price); // 2
var fc = new FancyCounter({ basePrice: 50 });
console.log("5.1.2", fc.price == c.price); // true
FancyCounter.prototype.priceString = function () { return "$: " + this.price; };
console.log("5.1.3", fc.priceString()); // $: 2
Counter.prototype.DOOR_FACTOR = 5;
console.log("5.1.4", c.price); // 2
// Counter.prototype.price = function() {return 17;};
console.log("5.1.5", c.price); // error
c._doors = 10;
console.log("5.1.6", c.price); // 10
