/**
 * Created by mstolze on 19/02/17.
 */

interface Point {
    readonly x: number;
    y: number;
}

let p:Point = {x: 1, y:2}; // TS Error: [  ] Ja [X ] Nein
//p.x = 2; 			       // TS Error: [X ] Ja [  ] Nein
p.y = 9;                   // TS Error: [  ] Ja [X ] Nein
//p.z = 7;                   // TS Error: [  ] Ja [X ] Nein

export default class Counter {
    private _doors: number;
    private _basePrice: number;
    public static readonly DOOR_FACTOR: number = 2;

    constructor({doors = 2, basePrice = 40}:{doors?: number, basePrice?: number} = {}) {
        this._doors = doors;
        this._basePrice = basePrice;
    }
    get price() {
        let priceFactor = Counter.DOOR_FACTOR * this._doors / 100;
        return priceFactor * this._basePrice;
    }
    priceString () {
        return "CHF: "+ this.price;
    }
}

class FancyCounter extends Counter {
    public static readonly DOOR_FACTOR = 3;
}

let c = new Counter({basePrice: 50});
console.log("5.1.1", c.price);              // 2

let fc = new FancyCounter({basePrice: 50});
console.log("5.1.2", fc.price == c.price);  // true

FancyCounter.prototype.priceString = function(){return "$: "+ this.price;};
console.log("5.1.3", fc.priceString());     // $: 2


Counter.prototype.DOOR_FACTOR = 5;
console.log("5.1.4", c.price);              // 2

// Counter.prototype.price = function() {return 17;};
console.log("5.1.5", c.price);              // error

c._doors = 10;
console.log("5.1.6", c.price);              // 10


