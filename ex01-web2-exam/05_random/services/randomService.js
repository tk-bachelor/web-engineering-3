function getCoinResult(count) {
        return Array(count).fill().map(() => !!Math.round(Math.random()));
}
module.exports = {getCoinResult : getCoinResult};