var express = require('express');
var path = require('path');

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var random = require('./routes/random');
require('./util/handlebarHelpers');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({ secret: 'casdklfjklöasdjfhuinckasdnf', resave: false, saveUninitialized: true}));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', require('./routes/random'));


// error handler
app.use(function(err, req, res, next) {
  res.locals.message = err.message;

  res.status(err.status || 500);
  res.render('error');
});
module.exports = app;

