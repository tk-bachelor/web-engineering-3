const express = require('express');
const router = express.Router();
const randomController = require("../controllers/randomController");

router.get('/', randomController.index);
router.get('/random', randomController.random);
router.get('/*', randomController.index);

module.exports = router;
