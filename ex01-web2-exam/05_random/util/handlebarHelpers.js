var hbs = require('hbs');

//Usage: {{?cond status 'error' 'error' 'ok'}}
hbs.registerHelper('?cond', function(c1, c2, value1, value2, options) {
    if(c1 === c2) {
        return value1;
    }
    return value2;
});

//Usage: {{? hasError 'error' 'ok'}}
hbs.registerHelper('?', function(exp, value1, value2, options) {
    if(exp) {
        return value1;
    }
    return value2;
});
