const defaultType = "chf";
const defaultCount = 2;

const randomService = require("../services/randomService");
const fs = require("fs");
const coinTypes = fs.readdirSync("./public/images/coins");  //contains: ['chf', 'eur']

function ensureSessionValue(req, sessionKey, defaultValue, newValue) {
  //TODO: Setzt (falls nötig) den newValue in die Session. Gibt, falls vorhanden, den Session Wert zurück ansonsten den defaultValue.

  if(typeof(newValue) != 'undefined') {
    if (defaultValue != newValue || req.session[sessionKey] != defaultValue) {
      req.session[sessionKey] = newValue
    }
  }
  return req.session[sessionKey] || defaultValue;
}

function getCountValue(req) {
  return ensureSessionValue(req, "count", defaultCount, req.query.count && Number(req.query.count));
}

function getCoinType(req) {
  let type = ensureSessionValue(req, "type", defaultType, req.query.type);
  return coinTypes.indexOf(type) >= 0 ? type : defaultType
}

function index(req, res) {
    res.render('index', {title: 'Flip Coin', count: getCountValue(req), coinTypes: coinTypes, type: getCoinType(req)});
}

function random(req, res, next) {
  let count = getCountValue(req);
  let type = getCoinType(req);

  if (count > 100) {
    next(new Error("Die Anzahl war ausserhalb des gültigen Bereichs"));
  }
  else {
    let coins = randomService.getCoinResult(count);
    res.render('random', {title: "Flip Coin Result", count: count, coins: coins, type: type});
  }
}

module.exports = {index, random};
