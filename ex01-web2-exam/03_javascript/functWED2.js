/**
 * Created by mstolze on 19/02/17.
 */
let data = [
    {name: "Michael", hobbies: ["chess", "hiking"]},
    {name: "Lisa", hobbies: ["chess", "cooking"]},
    {name: "Simon", hobbies: ["hiking", "cooking", "cinema"]},
    {name: "Bob", hobbies: ["hiking", "chess"]},
    {name: "Lea", hobbies: ["cinema", "cooking", "hiking"]},
];

function printNamesByHobby(hobby) {
    return data.filter(x => x.hobbies.indexOf(hobby) >= 0).map(x => x.name)
}

function printNamesByHobby2(hobby) {
    return data.reduce((found, dataRecord) =>
        (dataRecord.hobbies.indexOf(hobby) >= 0) ? [dataRecord.name, ...found] : found, []);
}


console.log(printNamesByHobby2("chess"));

function namesGroupedByHobbies() {
    return data.reduce((result, item) => {
        let key = item.hobbies.sort().join(",");
        result[key] = (result[key]) ? [...result[key], item.name] : [item.name];
        return result;
    }, {});
}

function printNamesWithSameHobbies() {
    const groups = namesGroupedByHobbies();
    return Object.keys(groups).map(key => {
        return {key: key, members: groups[key]}
    }).filter(x => x.members.length >= 2);
}

console.log(namesGroupedByHobbies());
console.log(printNamesWithSameHobbies());


