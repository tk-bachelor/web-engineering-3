import { Component, OnInit } from '@angular/core';
import {TitleService} from '../../core';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {
  private title: string = 'Not from service';

  constructor(titleService: TitleService){
    this.title = titleService.getTitle();
  }

  ngOnInit() {
  }

}
