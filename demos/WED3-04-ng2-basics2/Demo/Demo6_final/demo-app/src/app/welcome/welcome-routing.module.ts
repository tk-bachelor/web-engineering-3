import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {WelcomeComponent} from "./welcome.component";
import {WelcomeListComponent} from "./welcome-list.component";
import {WelcomeDetailComponent} from "./welcome-detail.component";

const routes: Routes = [
  {
    path: 'welcome',
    component: WelcomeComponent,
    children: [
      { path: ':id', component: WelcomeDetailComponent },
      { path: '', component: WelcomeListComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes) // !forChild() important
  ],
  exports: [
    RouterModule
  ]
})
export class WelcomeRoutingModule {}
