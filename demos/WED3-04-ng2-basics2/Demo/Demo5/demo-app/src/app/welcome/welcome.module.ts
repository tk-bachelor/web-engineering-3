import {NgModule, ModuleWithProviders} from '@angular/core';

import {SharedModule} from "../shared/shared.module";

import {WelcomeComponent} from "./welcome.component";

@NgModule({
  declarations: [
    WelcomeComponent
  ],
  imports: [
    SharedModule
  ],
  exports: [
    WelcomeComponent
  ],
  providers: [ ]
})
export class WelcomeModule {
  static forRoot(config?:{}) : ModuleWithProviders {
    return {
      ngModule: WelcomeModule,
      providers: [ ]
    };
  }

}
