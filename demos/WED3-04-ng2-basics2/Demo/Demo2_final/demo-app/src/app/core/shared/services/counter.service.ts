import {Injectable} from "@angular/core";
import {CounterModel} from "../models";

@Injectable()
export class CounterService {
  private model:CounterModel = new CounterModel();

  public load():CounterModel {
    return this.model;
  }

  public up():CounterModel {
    this.model.count++;
    return this.model;
  }
}
