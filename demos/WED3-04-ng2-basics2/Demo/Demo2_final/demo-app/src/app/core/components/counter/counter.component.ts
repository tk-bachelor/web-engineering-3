import { Component, OnInit, OnDestroy } from '@angular/core';
import {CounterModel, CounterService} from "../../shared";

@Component({
  selector: 'wed-counter',
  templateUrl: 'counter.component.html',
  styleUrls: ['counter.component.css']
})
export class CounterComponent implements OnInit, OnDestroy {

  private counter:CounterModel;

  constructor(private counterService:CounterService) {
    this.counter = counterService.load();
  }

  ngOnInit() {
    console.log("OnInit");
  }

  ngOnDestroy() {
    console.log("OnDestroy");
  }

  private up(event:UIEvent): void {
    this.counter = this.counterService.up();
    event.preventDefault();
  }
}
