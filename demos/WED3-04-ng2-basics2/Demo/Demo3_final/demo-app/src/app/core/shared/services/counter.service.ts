import {Injectable, EventEmitter} from "@angular/core";
import {CounterModel} from "../models";

@Injectable()
export class CounterService {
  private model:CounterModel;
  public modelChanged:EventEmitter<CounterModel> = new EventEmitter<CounterModel>();

  public load():void {
    this.model = new CounterModel();
    this.modelChanged.emit(this.model);
  }

  public up():void {
    this.model.count++;
    this.modelChanged.emit(this.model);
  }
}
