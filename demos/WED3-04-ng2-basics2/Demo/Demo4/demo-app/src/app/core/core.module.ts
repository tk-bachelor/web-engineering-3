import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import {SampleService, CounterService, CounterDataResourceService} from "./shared";
import { CounterComponent } from './components/counter/counter.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [CounterComponent],
  providers: [ ],
  exports: [ CounterComponent ]
})
export class CoreModule {
  static forRoot(config?: {}): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [ SampleService, CounterService, CounterDataResourceService ]
    };
  }
}
