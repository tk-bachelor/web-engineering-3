import {Http, Headers, Response, RequestOptionsArgs} from "@angular/http";
import {Injectable} from "@angular/core";

import {Observable} from "rxjs";

import {CounterModel} from "../models";

@Injectable()
export class CounterDataResourceService {
  private static SERVICE_URL:string = "http://localhost:3000/";
  private options:RequestOptionsArgs;

  constructor(private http:Http) {
    let headers = new Headers();
    headers.set("content-type", "application/json");
    this.options = { headers };
  }

  public get():Observable<CounterModel> {
    return null; // TODO
  }

  public sendUp():Observable<CounterModel>  {
    return null; // TODO
  }

  private handleError(e:Response|any) {
    return Observable.throw(e.message);
  }
}
