import {NgModule, ModuleWithProviders} from '@angular/core';

import {SharedModule} from "../shared/shared.module";

import {WelcomeRoutingModule} from "./welcome-routing.module";
import {WelcomeComponent} from "./welcome.component";
import {WelcomeListComponent} from "./welcome-list.component";
import {WelcomeDetailComponent} from "./welcome-detail.component";


@NgModule({
  declarations: [
    WelcomeComponent, WelcomeDetailComponent, WelcomeListComponent
  ],
  imports: [
    WelcomeRoutingModule, SharedModule
  ],
  exports: [
    WelcomeComponent
  ],
  providers: [ ]
})
export class WelcomeModule {
  static forRoot(config?:{}) : ModuleWithProviders {
    return {
      ngModule: WelcomeModule,
      providers: [ ]
    };
  }

}
