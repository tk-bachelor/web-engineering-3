import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {WelcomeComponent} from "./welcome.component";
import {WelcomeListComponent} from "./welcome-list.component";
import {WelcomeDetailComponent} from "./welcome-detail.component";

const routes: Routes = [
  {
    path: 'welcome',
    component: WelcomeComponent

    // TODO
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes) // !forChild() important
  ],
  exports: [
    RouterModule
  ]
})
export class WelcomeRoutingModule {}
