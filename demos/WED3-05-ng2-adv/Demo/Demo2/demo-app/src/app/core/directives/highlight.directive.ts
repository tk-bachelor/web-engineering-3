import { Directive, ElementRef, Input } from '@angular/core';

@Directive({ selector: '[wedHighlight]', exportAs:'highlight' })
export class HighlightDirective {
    constructor(el: ElementRef) {
       el.nativeElement.style.backgroundColor = this.color;
    }
	public get color() {
		return 'yellow';
	}
}
