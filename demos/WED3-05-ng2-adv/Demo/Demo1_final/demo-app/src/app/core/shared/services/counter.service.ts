import {Injectable, EventEmitter} from "@angular/core";
import {CounterModel} from "../models";
import {CounterDataResourceService} from "../resources";

@Injectable()
export class CounterService {
  public modelChanged:EventEmitter<CounterModel> = new EventEmitter<CounterModel>();

  constructor(private dataResource:CounterDataResourceService) {
  }

  public load():void {
    this.dataResource.get().subscribe(
      (data) => this.modelChanged.emit(data));
  }

  public up():void {
    this.dataResource.sendUp().subscribe(
      (data) => this.modelChanged.emit(data));
  }
}
