/**
 * Created by sgehrig on 07.03.2017.
 */
export class CounterModel {
  constructor(public count:number = 0, public team:string = "unspecified") {
  }

  public static fromDto(dto:any):CounterModel {
    return new CounterModel(dto.count, dto.team);
  }
}
