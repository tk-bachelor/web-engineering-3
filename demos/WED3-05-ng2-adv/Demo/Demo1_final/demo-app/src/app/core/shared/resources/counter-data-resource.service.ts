import {Http, Headers, Response, RequestOptionsArgs} from "@angular/http";
import {Injectable} from "@angular/core";

import {Observable} from "rxjs";

import {CounterModel} from "../models";

@Injectable()
export class CounterDataResourceService {
  private static SERVICE_URL:string = "http://localhost:3000/";
  private options:RequestOptionsArgs;

  constructor(private http:Http) {
    let headers = new Headers();
    headers.set("content-type", "application/json");
    this.options = { headers, withCredentials: true };
  }

  public get():Observable<CounterModel> {
    return this.http.get(CounterDataResourceService.SERVICE_URL + "api", this.options)
      .map((res) => CounterModel.fromDto(res.json()))
      .catch(this.handleError);
  }

  public sendUp():Observable<CounterModel>  {
    return this.http.post(CounterDataResourceService.SERVICE_URL + "api/up", void 0, this.options)
      .map((res) => CounterModel.fromDto(res.json()))
      .catch(this.handleError);
  }

  private handleError(e:Response|any) {
    return Observable.throw(e.message);
  }
}
