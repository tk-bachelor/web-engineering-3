import { Component, OnInit, OnDestroy } from '@angular/core';
import {CounterModel, CounterService} from "../../shared";
import {Subscription} from "rxjs";

@Component({
  selector: 'wed-counter',
  templateUrl: 'counter.component.html',
  styleUrls: ['counter.component.css']
})
export class CounterComponent implements OnInit, OnDestroy {

  private counter:CounterModel;
  private counterSubscription:Subscription;

  constructor(private counterService:CounterService) {
  }

  ngOnInit() {
    this.counterSubscription = this.counterService.modelChanged.subscribe((model:CounterModel) => {
      this.counter = model;
    });
    this.counterService.load();
  }

  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }

  private up(event:UIEvent): void {
    this.counterService.up();
    event.preventDefault();
  }
}
