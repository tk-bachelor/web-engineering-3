import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import {SampleService, CounterService, CounterDataResourceService} from "./shared";
import { CounterComponent } from './components';
import { LogoPipe } from './pipes';
import { HighlightDirective, ValidationDirective } from './directives';


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [CounterComponent, LogoPipe, HighlightDirective, ValidationDirective],
  providers: [ ],
  exports: [ CounterComponent, ValidationDirective ]
})
export class CoreModule {
  static forRoot(config?: {}): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [ SampleService, CounterService, CounterDataResourceService ]
    };
  }
}
