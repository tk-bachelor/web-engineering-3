import { Pipe, PipeTransform } from '@angular/core';

/*
 * Converts an input string into an animal logo, if a corresponding logo is present.

 * Takes a transformation argument that defaults to unspec.
 * Usage:
 *   value | logo:toImage
 * Example:
 *   {{ 'Bulls' |  logo:toImage}}
 *    formats to: <img src="app/core/assets/bulls.png">
 *   {{ 'Bulls' |  logo}}
 *    formats to: Butrue Bulls
 */
@Pipe({name:"logo", pure: true})
export class LogoPipe implements PipeTransform {
  private logos = {
    "Bulls": { toImage: 'app/core/assets/bulls.png', unspec: 'Butrue Bulls' },
    "Tigers": { toImage: 'app/core/assets/tigers.png', unspec: 'Langau Tigers' }
  };

  transform(value: string, transformSettings: string): string {
    if (this.logos[value]) {
      return (this.logos[value][transformSettings] || this.logos[value].unspec);
    }
    return value;
  }

}
