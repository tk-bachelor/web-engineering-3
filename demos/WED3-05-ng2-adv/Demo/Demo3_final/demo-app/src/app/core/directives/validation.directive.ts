import {Directive, forwardRef, Attribute, Input} from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[validateEqual][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => ValidationDirective), multi: true }
  ]
})
export class ValidationDirective implements Validator {
  @Input()
  public validateEqual: string;
  validate({value:modelValue}:AbstractControl): { [key: string]: any } {
    if (modelValue !== this.validateEqual) {
      return { validateEqual: false };
    }
    return null;
  }
}
