import { Directive, ElementRef, Input } from '@angular/core';

@Directive({ selector: '[wedHighlight]', exportAs:'highlight' })
export class HighlightDirective {
  private highlightColor:string = 'yellow';
  constructor(private el: ElementRef) {
  }
  @Input("wedHighlight")
  public set color(color:string) {
    this.highlightColor = color || this.highlightColor;
    this.setBackgroundColor(color);
  }
  public get color() {
    return this.highlightColor;
  }
  private setBackgroundColor(color:string):void {
    this.el.nativeElement.style.backgroundColor = this.color;
  }
}
