import { Component, OnInit } from '@angular/core';
import {CounterModel, CounterService} from "../../shared";

@Component({
  selector: 'wed-counter',
  templateUrl: 'counter.component.html',
  styleUrls: ['counter.component.css']
})
export class CounterComponent implements OnInit {

  private counter:CounterModel;

  constructor(private counterService:CounterService) {
    this.counter = counterService.load();
  }

  ngOnInit() {
  }

  private up(event:UIEvent): void {
    this.counter = this.counterService.up();
    event.preventDefault();
  }
}
