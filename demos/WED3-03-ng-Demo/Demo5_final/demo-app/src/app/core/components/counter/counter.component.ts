import { Component, OnInit } from '@angular/core';
import {CounterModel} from "../../shared/models";

@Component({
  selector: 'wed-counter',
  templateUrl: 'counter.component.html',
  styleUrls: ['counter.component.css']
})
export class CounterComponent implements OnInit {

  private counter:CounterModel = new CounterModel();

  constructor() { }

  ngOnInit() {
  }

  private up(event:UIEvent): void {
    this.counter.count++;
    event.preventDefault();
  }
}
