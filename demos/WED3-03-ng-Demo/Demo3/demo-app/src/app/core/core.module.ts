import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import {SampleService} from "./shared";

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [],
  providers: [ SampleService ]
})
export class CoreModule {
  static forRoot(config?: {}): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [ ]
    };
  }
}
