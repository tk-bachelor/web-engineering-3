import { Component, OnInit } from '@angular/core';
import {CounterModel} from "../../shared/models";

@Component({
  selector: 'wed-counter',
  templateUrl: 'counter.component.html',
  styleUrls: ['counter.component.css']
})
export class CounterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
