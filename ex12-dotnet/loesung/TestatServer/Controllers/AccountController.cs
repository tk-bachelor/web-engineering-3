using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestatServer.Models;
using TestatServer.Models.ViewModels;
using TestatServer.Services;

namespace TestatServer.Controllers
{

    [Route("accounts")]
    [Authorize]
    public class AccountController : Controller
    {
        private readonly AccountService _accountService;

        public AccountController(AccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet]
        public AccountOwnerViewModel Get()
        {
            var accountNr = User.FindFirst(SecurityClaims.AccountIdClaim).Value;
            var account = _accountService.GetAccount(accountNr);

            return new AccountOwnerViewModel()
            {
                AccountNr = accountNr,
                Amount = account.Amount,
                OwnerId = account.Owner.Id,
                Owner = new OwnerViewModel() { Login = account.Owner.UserName, AccountNr = accountNr, Firstname = account.Owner.FirstName, Lastname = account.Owner.LastName }
            }; 
        }



        [HttpGet("{accountNr}")]
        public AccountViewModel Get(string accountNr)
        {
            var account = _accountService.GetAccount(accountNr);
            return new AccountViewModel()
            {
                AccountNr = accountNr,
                Owner = new UserViewModel() {Firstname = account.Owner.FirstName, Lastname = account.Owner.LastName}
            };
        }



        [HttpPost("transactions")]
        public TransactionResultViewModel AddTransaction([FromBody] TransactionViewModel newTransaction)
        {
            var transaction = _accountService.AddTransaction(User.FindFirst(SecurityClaims.AccountIdClaim).Value, newTransaction.Target, newTransaction.Amount, DateTime.Now);
            return new TransactionResultViewModel()
            {
                Amount = transaction.Amount,
                Date = transaction.Date,
                Target = transaction.Target,
                From = transaction.From,
                Id = transaction.Id,
                Total = _accountService.GetAccount(User.FindFirst(SecurityClaims.AccountIdClaim).Value).Amount
            };
        }

        [HttpGet("transactions")]
        public TransactionSearchResult GetTransactions([FromQuery]TransactionSearchQuery query)
        {
            return _accountService.GetTransactions(User.FindFirst(SecurityClaims.AccountIdClaim).Value, query);
        }
    }
}