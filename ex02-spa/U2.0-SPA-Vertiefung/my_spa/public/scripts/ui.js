/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ 2:
/***/ (function(module, exports) {

(function ($, ui) {
    /**
     * Routing
     */
    var Router = (function () {
        /**
         * @param routeConfig Supported format:
         * {
         *  rootPath: String,
         *  initialRoute: String,
         *  routes: { [String]: Function }
         * }
         */
        function Router(routeConfig) {
            this.routeConfig = routeConfig;
        }
        Router.prototype.navigate = function (route) {
            window.history.pushState(null, void 0, route);
            this.activate(route);
        };
        Router.prototype.activate = function (route) {
            if (this.routeConfig.routes[route]) {
                this.routeConfig.routes[route]();
            }
        };
        Router.prototype.initialize = function () {
            var activatedRoute = self.location.pathname;
            if (self.location.pathname.indexOf(this.routeConfig.rootPath) == 0) {
                activatedRoute = self.location.pathname.substring(this.routeConfig.rootPath.length);
            }
            this.activate(activatedRoute || this.routeConfig.initialRoute);
        };
        return Router;
    }());
    /**
     * MVC
     */
    var CounterController = (function () {
        function CounterController(counterService) {
            this.counterService = counterService;
            this.indexTemplateCompiled = Handlebars.compile($('#index-view').html());
        }
        CounterController.prototype.indexAction = function (viewRef) {
            var _this = this;
            this.counterService.load(function (model) {
                _this.renderIndexView(viewRef, model);
            });
            $(viewRef).on('click', '[data-click=up]', function (e) {
                _this.counterService.up(function (model) {
                    _this.renderIndexView(viewRef, model);
                });
                e.preventDefault();
            });
        };
        CounterController.prototype.renderIndexView = function (viewRef, model) {
            viewRef.html(this.indexTemplateCompiled({ counter: model }));
        };
        return CounterController;
    }());
    // exports
    ui.CounterController = CounterController;
    ui.Router = Router;
})(jQuery, window['ui'] = window['ui'] || {});


/***/ })

/******/ });
//# sourceMappingURL=ui.js.map