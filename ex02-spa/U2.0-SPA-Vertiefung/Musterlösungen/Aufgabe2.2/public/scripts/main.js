/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Routing
 */
var Router = (function () {
    /**
     * @param routeConfig Supported format:
     * {
     *  rootPath: String,
     *  initialRoute: String,
     *  routes: { [String]: Function }
     * }
     */
    function Router(routeConfig) {
        this.routeConfig = routeConfig;
    }
    Router.prototype.navigate = function (route) {
        window.history.pushState(null, void 0, route);
        this.activate(route);
    };
    Router.prototype.activate = function (route) {
        if (this.routeConfig.routes[route]) {
            this.routeConfig.routes[route]();
        }
    };
    Router.prototype.initialize = function () {
        var activatedRoute = self.location.pathname;
        if (self.location.pathname.indexOf(this.routeConfig.rootPath) == 0) {
            activatedRoute = self.location.pathname.substring(this.routeConfig.rootPath.length);
        }
        this.activate(activatedRoute || this.routeConfig.initialRoute);
    };
    return Router;
}());
exports.Router = Router;
/**
 * MVC
 */
var CounterController = (function () {
    function CounterController(counterService) {
        this.counterService = counterService;
        this.indexTemplateCompiled = Handlebars.compile($('#index-view').html());
    }
    CounterController.prototype.indexAction = function (viewRef) {
        var _this = this;
        this.counterService.load(function (model) {
            _this.renderIndexView(viewRef, model);
        });
        $(viewRef).on('click', '[data-click=up]', function (e) {
            _this.counterService.up(function (model) {
                _this.renderIndexView(viewRef, model);
            });
            e.preventDefault();
        });
    };
    CounterController.prototype.renderIndexView = function (viewRef, model) {
        viewRef.html(this.indexTemplateCompiled({ counter: model }));
    };
    return CounterController;
}());
exports.CounterController = CounterController;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Model
 */
var CounterModel = (function () {
    function CounterModel(team, count) {
        this.team = team || "unspecified";
        this.count = count || 0;
    }
    CounterModel.fromDto = function (dto) {
        return new CounterModel(dto.team, dto.count);
    };
    return CounterModel;
}());
exports.CounterModel = CounterModel;
/**
 * Service
 */
var CounterService = (function () {
    function CounterService(counterDataResource) {
        this.counterDataResource = counterDataResource;
    }
    CounterService.prototype.load = function (callback) {
        this.counterDataResource.get(function (dto) {
            callback(CounterModel.fromDto(dto));
        });
    };
    CounterService.prototype.up = function (callback) {
        this.counterDataResource.sendUp(function (dto) {
            callback(CounterModel.fromDto(dto));
        });
    };
    return CounterService;
}());
exports.CounterService = CounterService;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var CounterDataResource = (function () {
    function CounterDataResource() {
    }
    CounterDataResource.prototype.get = function (callback) {
        $.get('/api', function (data) {
            callback(data);
        });
    };
    CounterDataResource.prototype.sendUp = function (callback) {
        $.post('/api/up', function (data) {
            callback(data);
        });
    };
    return CounterDataResource;
}());
exports.CounterDataResource = CounterDataResource;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var ui_1 = __webpack_require__(0);
var dl_1 = __webpack_require__(2);
var bl_1 = __webpack_require__(1);
var ui_2 = __webpack_require__(0);
// bootstrap
$(function () {
    var counterDataResource = new dl_1.CounterDataResource();
    var counterService = new bl_1.CounterService(counterDataResource);
    var controller = new ui_2.CounterController(counterService);
    var routerOutlet = $("#appContainer");
    var router = new ui_1.Router({
        rootPath: "/",
        initialRoute: "index",
        routes: {
            "index": function () { controller.indexAction(routerOutlet); }
        }
    });
    router.initialize();
});


/***/ })
/******/ ]);
//# sourceMappingURL=main.js.map