import {Router} from './ui';
import {CounterDataResource} from './dl';
import {CounterService} from './bl';
import {CounterController} from './ui';

// bootstrap
$(function() {
	let counterDataResource = new CounterDataResource();
	let counterService = new CounterService(counterDataResource);
	let controller = new CounterController(counterService);

	let routerOutlet = $("#appContainer");
	let router = new Router({
		rootPath: "/",
		initialRoute: "index",
		routes: {
			"index": () => { controller.indexAction(routerOutlet); }
		}
	});
	router.initialize();
});
