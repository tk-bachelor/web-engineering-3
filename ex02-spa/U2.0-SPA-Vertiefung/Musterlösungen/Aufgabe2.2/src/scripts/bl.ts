import {ICounterDataResource} from "./dl";

export interface ICounterService {
    load(callback:(data:any)=>void):void;
    up(callback:(data:any)=>void):void;
}

/**
 * Model
 */
export class CounterModel {
    private team:string;
    private count:number;
    constructor(team:string, count:number) {
        this.team = team || "unspecified";
        this.count = count || 0;
    }
    static fromDto(dto:any):CounterModel {
        return new CounterModel(dto.team, dto.count);
    }
}

/**
 * Service
 */
export class CounterService implements ICounterService {
    private counterDataResource;
    constructor(counterDataResource:ICounterDataResource) {
        this.counterDataResource = counterDataResource;
    }
    load(callback:(data:any)=>CounterModel):void {
        this.counterDataResource.get((dto) => {
            callback(CounterModel.fromDto(dto));
        });
    }
    up(callback:(data:any)=>CounterModel):void {
        this.counterDataResource.sendUp((dto) => {
            callback(CounterModel.fromDto(dto));
        });
    }
}
