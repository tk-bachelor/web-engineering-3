import {container} from './di';

import {Router} from './ui';
import {ICounterController} from './ui';

// bootstrap
$(function() {
	let routerOutlet = $("#appContainer");
	let router = new Router({
		rootPath: "/",
		initialRoute: "index",
		routes: {
			"index": () => { container.resolve<ICounterController>("CounterController").indexAction(routerOutlet); }
		}
	});
	router.initialize();
});
