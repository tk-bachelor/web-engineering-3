interface Registration<T> {
    name:string,
    dependencies:string[],
    type:new (...args: any[]) => T,
    instance: any
}
/**
 * Dependency Injection
 */
export class Container {
    private registrations:{[key:string]:Registration<any>} = { };
    constructor() {
    }
    register<T>(name:string, dependencies:string[], type:new (...args: any[]) => T):void {
        this.registrations[name] = { name, dependencies, type, instance: null };
    }
    resolve<T>(name):T {
        if (!this.registrations[name].instance) {
            this.registrations[name].instance = new this.registrations[name].type(...this.resolveDependencies(name));
        }
        return <T>this.registrations[name].instance;
    }
    resolveDependencies(name):any[] {
        let dependencies = [ ];
        if (this.registrations[name].dependencies) {
            this.registrations[name].dependencies.forEach(r => {
                dependencies.push(this.resolve<any>(r));
            });
        }
        return dependencies;
    }
}

export let container = new Container();